/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig2.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import no.uib.info233.v2016.sto020_can013.oblig2.issuetracker.IssueTracker;

/**
 * A Utility class mostly for saving and loading data from a .dat file
 * @author sto020
 *
 */
public class Utils {

	
	/**
	 * Loads data from database.dat if exists
	 * 
	 * @return boolean - if load successful
	 * 
	 */
	public static IssueTracker loadData() {
		IssueTracker load = null;

		// unserialize the Queue
		System.out.println("unserializing issueTracker");
		try {

			FileInputStream fin = new FileInputStream("database.dat");
			ObjectInputStream ois = new ObjectInputStream(fin);
			load = (IssueTracker) ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
			// e.printStackTrace();
			System.out.println("No file to load, creating new");
			load = new IssueTracker();
		} catch (Exception ex) {
			System.out.println("Error reading file, creating new");

			load = new IssueTracker();
		}

		return load;

	}

	/**
	 * Saves the data to database.dat with serialization
	 * 
	 * @param issueTracker
	 *            the IssueTracker object
	 * @return boolean - if saved successfully or not
	 */
	public static boolean saveData(IssueTracker issueTracker) {

		// serialize the Queue
		System.out.println("serializing issuetracker");
		try {
			FileOutputStream fout = new FileOutputStream("database.dat", false);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(issueTracker);
			oos.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
	
	/**
	 * This method will convert a String input of Numbers to an int
	 * 
	 * @param value
	 *            The string of numbers to convert to int
	 * @return The parsed string converted to int, will return 0 if error
	 */
	public static int StringToInteger(String value) {

		try {
			return Integer.parseInt(value.trim());

		} catch (NumberFormatException nfe) {
			System.out.println("Error parsing String to Integer. Please check input, returning '0'");
			return 0;
		}

	}

	
	/**
	 * Creates a MDF hash of a string input, used for saving and comparing passwords
	 * @param value The String/Password to create hash of
	 * @return The finished hashed password for usage and saving
	 */
	public static String StringHash(String value) {

		try {
			// Create an instance for MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			// Add password bytes to the hash creator
			md.update(value.getBytes());
			// Get the bytes from the hash
			byte[] bytes = md.digest();
			// Convert the byte hash to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			// return the hashed password in hex format
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return null;

	}
}
