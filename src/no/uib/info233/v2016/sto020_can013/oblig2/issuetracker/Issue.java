/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig2.issuetracker;

import java.io.Serializable;

/**
 * Represents an Issue
 * @author sto020
 *
 */
public class Issue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2932297150690670593L;
	int ID;
	int priority = 0;
	User assignedUser = null;
	String createdDate = null;
	String descriptiveText = null;
	String location = null;

	/**
	 * Creates an Issue object
	 * 
	 * @param ID
	 *            The issue id
	 * @param priority
	 *            The priority of the issue
	 * @param assignedUser
	 *            The assigned Employee/User
	 * @param createdDate
	 *            The date the issue was created
	 * @param savedText
	 *            The issue descriptive text
	 * @param location
	 *            The location of the Issue
	 */
	public Issue(int ID, int priority, User assignedUser, String createdDate, String descriptiveText, String location) {
		this.ID = ID;
		this.priority = priority;
		this.assignedUser = assignedUser;
		this.createdDate = createdDate;
		this.descriptiveText = descriptiveText;
		this.location = location;
	}

	public Issue() {

	}

	/*
	 * Overrides the toString of Issue to give a nice string in return
	 */
	@Override
	public String toString() {
		return String.format("Issue %s, %s: Priority %s, %s. %s. %s\n", this.ID, this.createdDate, this.priority,
				this.descriptiveText, this.location, this.assignedUser);

	}

	/**
	 * Get the id number for the issue
	 * 
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * Set the id number for the issue
	 * 
	 * @param iD
	 *            the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority
	 *            the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * Get the User assigned to fix the issue
	 * 
	 * @return the assignedUser
	 */
	public User getAssignedUser() {
		return assignedUser;
	}

	/**
	 * Set the User assigned to fix the issue
	 * 
	 * @param assignedUser
	 *            the assignedUser to set
	 */
	public void setAssignedUser(User assignedUser) {
		this.assignedUser = assignedUser;
	}

	/**
	 * Get the date this issue was created
	 * 
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}

	/**
	 * Set the date this issue was created
	 * 
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Get the descriptive text of the issue
	 * 
	 * @return the savedText
	 */
	public String getDescriptiveText() {
		return descriptiveText;
	}

	/**
	 * Set a descriptive text for the issue
	 * 
	 * @param savedText
	 *            the savedText to set
	 */
	public void setDescriptiveText(String descriptiveText) {
		this.descriptiveText = descriptiveText;
	}

	/**
	 * Get the location of the issue
	 * 
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Set the location of the issue
	 * 
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

}
