package no.uib.info233.v2016.sto020_can013.oblig2.issuetracker;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * A Custom model to be used with JTables, uses the AbstractTableModel
 * @author sto020
 * @author can013
 */
public class IssueModel extends AbstractTableModel {

	String[] columnNames = { "ID", "Priority Level", "Creation Date", "Description", "Location", "User" }; // Array of names for the columns.

	private static final long serialVersionUID = 1L;
	
	private List<Issue> issues; // The array list of all issues.
	
	/**
	 * Constructor for the IssueModel class
	 * @param issueList the list of issues to be set to field array list.
	 */
	public IssueModel(List<Issue> issueList) {
		this.issues = issueList;
	}

	/* 
	 * @return the number of rows in the array.
	 */
	@Override
	public int getRowCount() {
		return this.issues == null ? 0 : this.issues.size();
	}

	/* 
	 * @return 6 the static column count.
	 */
	@Override
	public int getColumnCount() {
		return 6;
	}

	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	/* 
	 * @param col the number of columns.
	 * @return 
	 */
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/* 
	 * @param rowIndex the index of the row.
	 * @param columnIndex the index of the column.
	 * returns the value at the selected rowIndex and
	 * columnIndex.
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		Object value = null;
		Issue issue = issues.get(rowIndex);
		switch (columnIndex) {
		case 0:
			value = issue.getID();
			break;
		case 1:
			value = issue.getPriority();
			break;
		case 2:
			value = issue.getCreatedDate();
			break;
		case 3:
			value = issue.getDescriptiveText();
			break;
		case 4:
			value = issue.getLocation();
			break;
		case 5:
			value = issue.getAssignedUser().getUsername();
			break;
		}
		return value;
	}

	public String getIssueIdAt(int index) {
		return (String) getValueAt(index, 0);
	}

	/**
	 * @param row the row to remove.
	 * method removes the selected row.
	 */
	public void removeRow(int row) {
		if (issues == null)
			return;
		issues.remove(row);
	}

	/**
	 * @param issue the issue to add to a new row.
	 * method adds a new issue to an available row.
	 */
	public void addRow(Issue issue) {
		if (issues == null)
			this.issues = new ArrayList<Issue>();

		issues.add(issue);
	}
}