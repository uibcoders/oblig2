package no.uib.info233.v2016.sto020_can013.oblig2.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.List;
import java.awt.Toolkit;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

import no.uib.info233.v2016.sto020_can013.oblig2.gui.panels.DataPanel;
import no.uib.info233.v2016.sto020_can013.oblig2.gui.panels.DatePicker;

import javax.swing.JSplitPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Creates the main GUI
 * 
 * @author can013
 * @author sto020
 * @version 0.2.0
 */
public class GUI extends JFrame {

	/**gygubvyvhgvhvhn
	 * 
	 */
	private static final long serialVersionUID = -7513854125040781915L;
	private JPanel contentPane; // Panel
	private DataPanel searchDPanel; // the JTable to be in the search tab.
	private DataPanel allIssuesPanel; // the JTable to be in the allIssues tab.
	private DatePicker dtTo; // field to limit the date search.
	private DatePicker dtFrom; // field to search after issues from creation
								// date and onwards.
	private JComboBox<String> userBox; // dropdown menu for different search
										// options.
	private JButton btnUpdate; // updates the search field in the search tab.
	private JTabbedPane tabbedPane;
	private JButton btnNewIssue; // button to add new issues to allIssues tab.
	private List userList;
	private JTextField searchByIDField;
	private JMenuItem menuImportData; // menuitem for importing xml file.
	private JMenuItem menuExitAppliction; // menuitem to exit the application.
	private JMenuItem menuSettingsAddUser; // menuitems to add user.
	private JButton btnGetHighPriority; // button to retrieve high priority
										// issues.
	private JSplitPane splitPane; // splits the pane.
	private static GUI instance = null;

	/**
	 * Create the frame.
	 * 
	 * @param title
	 *            the title of the GUI frame. Sets up the graphical interface,
	 *            adds relevant items onto it.
	 */
	public GUI(String title) {
		setTitle(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Double width = screenSize.getWidth() / 2;
		Double height = screenSize.getHeight() / 2;

		setBounds(100, 100, width.intValue(), height.intValue());
		// center on user screen, because why not
		setLocation(screenSize.width / 2 - this.getSize().width / 2, screenSize.height / 2 - this.getSize().height / 2);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Search", null, panel, null);

		Calendar rightNow = Calendar.getInstance();
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);

		JLabel lblNewLabel = new JLabel("Show Issues from:"); // Creates a new
																// JLabel and
																// put
																// constraints
																// onto it.
		sl_panel.putConstraint(SpringLayout.NORTH, lblNewLabel, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, lblNewLabel, 35, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.EAST, lblNewLabel, -399, SpringLayout.EAST, panel);
		panel.add(lblNewLabel);

		btnUpdate = new JButton("Show me");
		panel.add(btnUpdate);
		dtFrom = new DatePicker(rightNow);
		sl_panel.putConstraint(SpringLayout.NORTH, dtFrom, 5, SpringLayout.SOUTH, lblNewLabel);
		sl_panel.putConstraint(SpringLayout.WEST, dtFrom, 57, SpringLayout.WEST, panel);

		panel.add(dtFrom);
		JLabel lblTo = new JLabel("to");
		sl_panel.putConstraint(SpringLayout.NORTH, lblTo, 28, SpringLayout.SOUTH, lblNewLabel);
		sl_panel.putConstraint(SpringLayout.WEST, lblTo, 41, SpringLayout.EAST, dtFrom);
		lblTo.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel.add(lblTo);

		dtTo = new DatePicker(rightNow);
		sl_panel.putConstraint(SpringLayout.WEST, dtTo, 38, SpringLayout.EAST, lblTo);
		sl_panel.putConstraint(SpringLayout.SOUTH, dtTo, 0, SpringLayout.SOUTH, dtFrom);

		panel.add(dtTo);

		searchDPanel = new DataPanel(null);
		sl_panel.putConstraint(SpringLayout.SOUTH, btnUpdate, -6, SpringLayout.NORTH, searchDPanel);
		sl_panel.putConstraint(SpringLayout.NORTH, searchDPanel, 144, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, searchDPanel, 5, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, searchDPanel, 0, SpringLayout.SOUTH, panel);
		sl_panel.putConstraint(SpringLayout.EAST, searchDPanel, -5, SpringLayout.EAST, panel);
		panel.add(searchDPanel);

		userBox = new JComboBox<String>();
		sl_panel.putConstraint(SpringLayout.WEST, btnUpdate, 17, SpringLayout.EAST, userBox);
		sl_panel.putConstraint(SpringLayout.SOUTH, userBox, -6, SpringLayout.NORTH, searchDPanel);
		panel.add(userBox);

		searchByIDField = new JTextField(5);
		JLabel idLabelSearch = new JLabel("Search by ID : ");
		sl_panel.putConstraint(SpringLayout.SOUTH, searchByIDField, -5, SpringLayout.NORTH, searchDPanel);
		sl_panel.putConstraint(SpringLayout.EAST, searchByIDField, 200, SpringLayout.EAST, btnUpdate);
		sl_panel.putConstraint(SpringLayout.EAST, idLabelSearch, 140, SpringLayout.EAST, btnUpdate);
		sl_panel.putConstraint(SpringLayout.SOUTH, idLabelSearch, -8, SpringLayout.NORTH, searchDPanel);
		panel.add(searchByIDField);
		panel.add(idLabelSearch);

		JLabel lblFilterBy = new JLabel("Filter by:");
		sl_panel.putConstraint(SpringLayout.SOUTH, lblFilterBy, -11, SpringLayout.NORTH, searchDPanel);
		sl_panel.putConstraint(SpringLayout.WEST, userBox, 6, SpringLayout.EAST, lblFilterBy);
		sl_panel.putConstraint(SpringLayout.EAST, userBox, 147, SpringLayout.EAST, lblFilterBy);
		sl_panel.putConstraint(SpringLayout.WEST, lblFilterBy, 0, SpringLayout.WEST, searchDPanel);
		panel.add(lblFilterBy);

		btnGetHighPriority = new JButton("Get HIGH priority");
		sl_panel.putConstraint(SpringLayout.SOUTH, btnGetHighPriority, -6, SpringLayout.NORTH, searchDPanel);
		sl_panel.putConstraint(SpringLayout.EAST, btnGetHighPriority, -10, SpringLayout.EAST, panel);
		panel.add(btnGetHighPriority);

		splitPane = new JSplitPane();
		tabbedPane.addTab("All Issues", null, splitPane, null);

		btnNewIssue = new JButton("New issue");
		btnNewIssue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		splitPane.setLeftComponent(btnNewIssue);

		allIssuesPanel = new DataPanel(null);
		splitPane.setRightComponent(allIssuesPanel);

		JScrollPane scrollPane = new JScrollPane();
		tabbedPane.addTab("All Users", null, scrollPane, null);

		userList = new List();
		scrollPane.setViewportView(userList);

		JLabel lblAllUsers = new JLabel("All Users");

		scrollPane.setColumnHeaderView(lblAllUsers);
		getContentPane().add(contentPane);

		JMenuBar menuBar = new JMenuBar(); // menubar at the top of the frame.
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("File"); // creates a new JMenu and adds it
												// to menuBar.
		menuBar.add(mnNewMenu);

		menuImportData = new JMenuItem("Import from XML");
		mnNewMenu.add(menuImportData);

		menuExitAppliction = new JMenuItem("Exit");
		menuExitAppliction.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();

			}
		});

		mnNewMenu.add(menuExitAppliction);

		JMenu mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);

		menuSettingsAddUser = new JMenuItem("Add user");
		mnSettings.add(menuSettingsAddUser);
		setVisible(true);
	}

	public static synchronized GUI getInstance() {
		if (instance == null) {
			instance = new GUI("");
		}
		return instance;
	}

	/**
	 * @return the searchDpanel an instance of the DataPanel class.
	 */

	public DataPanel getsearchDPanel() {
		return searchDPanel;
	}

	/**
	 * @return allIssuesPanel an instance of the DataPanel class.
	 */
	public DataPanel getAllIssuesPanel() {
		return allIssuesPanel;
	}

	/**
	 * @return dtFrom an instance of the DatePicker class.
	 */
	public DatePicker getDtFrom() {
		return dtFrom;
	}

	/**
	 * @param dtFrom
	 *            instance of DatePicker to be the new value of field dtFrom.
	 */
	public void setDtFrom(DatePicker dtFrom) {
		this.dtFrom = dtFrom;
	}

	/**
	 * @return dtTo the instance of the DatePicker class.
	 */
	public DatePicker getDtTo() {
		return dtTo;
	}

	/**
	 * @param dtTo
	 *            instance of the DatePicker class to be the new value of field
	 *            dtTo.
	 */
	public void setDtTo(DatePicker dtTo) {
		this.dtTo = dtTo;
	}

	/**
	 * @return the btnUpdate button.
	 */
	public JButton getBtnUpdate() {
		return btnUpdate;
	}

	public JTextField getSearchByIDField() {
		return this.searchByIDField;
	}

	/**
	 * @param btnUpdate
	 *            to be the new value of btnUpdate field.
	 */
	public void setBtnUpdate(JButton btnUpdate) {
		this.btnUpdate = btnUpdate;
	}

	/**
	 * @return the userBox.
	 */
	public JComboBox<String> getUserBox() {
		return userBox;
	}

	/**
	 * @return userList the list of users.
	 */
	public List getUserList() {
		return userList;
	}

	/**
	 * @return the tabbedPane.
	 */
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	/**
	 * @return the menuImportData
	 */
	public JMenuItem getMenuImportData() {
		return menuImportData;
	}

	/**
	 * @return the btnNewIssue button.
	 */
	public JButton getBtnNewIssue() {
		return btnNewIssue;
	}

	/**
	 * @return the menuExitAppliction
	 */
	public JMenuItem getMenuExitAppliction() {
		return menuExitAppliction;
	}

	/**
	 * @return the menuSettingsAddUser
	 */
	public JMenuItem getMenuSettingsAddUser() {
		return menuSettingsAddUser;
	}

	/**
	 * @return the btnGetHighPriority
	 */
	public JButton getBtnGetHighPriority() {
		return btnGetHighPriority;
	}
}
