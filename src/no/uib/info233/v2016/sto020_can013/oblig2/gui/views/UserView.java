package no.uib.info233.v2016.sto020_can013.oblig2.gui.views;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import no.uib.info233.v2016.sto020_can013.oblig2.gui.GUI;
import no.uib.info233.v2016.sto020_can013.oblig2.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig2.issuetracker.User;
import no.uib.info233.v2016.sto020_can013.oblig2.main.Main;
import no.uib.info233.v2016.sto020_can013.oblig2.utilities.Utils;

public class UserView extends JFrame {

	private static final long serialVersionUID = -4062320114203994593L;
	private JPanel contentPane;
	private JLabel labelUsername, lblUsername, lblFirstName, lblLastName;
	private JTextField textFieldUsername;
	private JTextField textFieldFirstname;
	private JTextField textFieldLastname;
	private JButton btnSave;
	private JButton btnClose;
	private IssueTracker iTracker;
	private User currentUser;
	private JLabel lblOldPassword;
	private JTextField textFieldOldPassword;
	private JTextField textFieldNewPassword;
	private JLabel lblNewPassword;
	private boolean newUser = false;

	/**
	 * Constructor for the UserView, initializes all necessary components and
	 * classes
	 * 
	 * @param user
	 * @param iTracker
	 * @param setup
	 */
	public UserView(User user, IssueTracker iTracker) {
		this.iTracker = iTracker;
		this.currentUser = user;

		newUser = this.currentUser.getUsername().isEmpty();

		if (!newUser)
			setTitle("Editing user: " + this.currentUser.getUsername());
		else
			setTitle("Creating new user");

		createComponents();
		addComponents();

		appendListeners();

		init();

	}

	/*
	 * Sets default close ,location and makes the View visible
	 */
	private void init() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		if (!newUser)
			setUserData();
		setLocationRelativeTo(GUI.getInstance());
		setVisible(true);
	}

	/*
	 * Creates the components of for the UserView
	 */
	private void createComponents() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 2));

		lblUsername = new JLabel("Username:");

		if (!newUser) {
			labelUsername = new JLabel();

			labelUsername.setToolTipText("Username\r\n");
		} else {
			textFieldUsername = new JTextField(15);
			textFieldUsername.setToolTipText("Username\r\n");
		}

		lblFirstName = new JLabel("First Name:");

		textFieldFirstname = new JTextField(15);
		textFieldFirstname.setToolTipText("First Name");

		lblLastName = new JLabel("Last Name:");

		textFieldLastname = new JTextField(15);
		textFieldLastname.setToolTipText("Last Name");

		btnClose = new JButton("Close");
		btnSave = new JButton("Save");

		lblOldPassword = new JLabel("Old password:");

		textFieldOldPassword = new JPasswordField(15);

		lblNewPassword = new JLabel("New Password:");

		textFieldNewPassword = new JPasswordField(15);
	}

	/*
	 * Adds the components created by createComponents to the contentPane
	 */
	private void addComponents() {
		contentPane.add(lblUsername);

		if (!newUser)
			contentPane.add(labelUsername);

		if (newUser)
			contentPane.add(textFieldUsername);

		contentPane.add(lblFirstName);

		contentPane.add(textFieldFirstname);

		contentPane.add(lblLastName);

		contentPane.add(textFieldLastname);

		if (!newUser)
			contentPane.add(lblOldPassword);

		if (!newUser)
			contentPane.add(textFieldOldPassword);

		contentPane.add(lblNewPassword);

		contentPane.add(textFieldNewPassword);

		contentPane.add(btnClose);
		contentPane.add(btnSave);
	}

	/*
	 * Adds listeners and actions to the buttons
	 */
	private void appendListeners() {
		btnClose.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});

		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveUserData();
			}
		});
	}

	/*
	 * Sets the textfields of the View to the User (currentUser) data
	 */
	private void setUserData() {

		labelUsername.setText(this.currentUser.getUsername());

		textFieldFirstname.setText(this.currentUser.getFirstName());

		textFieldLastname.setText(this.currentUser.getLastName());

	}

	/*
	 * Saves the newly created or edited user to the IssueTracker userList
	 */
	private void saveUserData() {

		if (newUser) { // if a new user check if the username exist already
			this.currentUser.setUsername(textFieldUsername.getText().trim());
			if (this.iTracker.checkUserExist(this.currentUser.getUsername())) {
				JOptionPane.showMessageDialog(this,
						String.format("Username '%s' already exist!", this.currentUser.getUsername()));
				return;
			}
		}

		// remove the user from the collection to get ready to add the updated
		// one
		if (this.iTracker.removeUser(currentUser)) {

			this.currentUser.setFirstName(textFieldFirstname.getText());
			this.currentUser.setLastName(textFieldLastname.getText());

			if (!textFieldNewPassword.getText().isEmpty()) {
				if ((textFieldOldPassword.getText().isEmpty() && currentUser.getLoginPasswordHash().isEmpty())
						|| (currentUser.getLoginPasswordHash()
								.equals(Utils.StringHash(textFieldOldPassword.getText())))) {
					this.currentUser.setLoginPassword(textFieldNewPassword.getText());
				}

				else {
					this.iTracker.addUser(currentUser);
					JOptionPane.showMessageDialog(this, "Enter the correct old password");
					return;
				}
			} else {
				// transfer password
				this.currentUser.setLoginPasswordHash(currentUser.getLoginPasswordHash());
			}
			this.iTracker.addUser(this.currentUser);
			Utils.saveData(iTracker);
			Main.updateUserLists();
		}

		setVisible(false);
		dispose();
	}

	/**
	 * @return the labelUsername
	 */
	public JLabel getLabelUsername() {
		return labelUsername;
	}

	/**
	 * @return the textFieldFirstname
	 */
	public JTextField getTextFieldFirstname() {
		return textFieldFirstname;
	}

	/**
	 * @return the textFieldLastname
	 */
	public JTextField getTextFieldLastname() {
		return textFieldLastname;
	}

	/**
	 * @return the btnSave
	 */
	public JButton getBtnSave() {
		return btnSave;
	}

	/**
	 * @return the btnClose
	 */
	public JButton getBtnClose() {
		return btnClose;
	}

}
