package no.uib.info233.v2016.sto020_can013.oblig2.gui.views;

import java.awt.Canvas;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import no.uib.info233.v2016.sto020_can013.oblig2.gui.GUI;
import no.uib.info233.v2016.sto020_can013.oblig2.issuetracker.Issue;
import no.uib.info233.v2016.sto020_can013.oblig2.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig2.issuetracker.User;
import no.uib.info233.v2016.sto020_can013.oblig2.main.Main;
import no.uib.info233.v2016.sto020_can013.oblig2.utilities.Utils;
import java.awt.Font;


/**
 * Creates a view to create and edit an Issue, it also updates the main GUI lists
 * @author Sto020
 * @author can013
 */
public class IssueView extends Canvas {

	private static final long serialVersionUID = 1L;

	private JButton closeButton; // button for closing the issue view
	private JButton saveButton; // button to save information.
	private JFrame frame; // frame for the issue view.
	private Issue issue; // particular issue to be shown.
	private IssueTracker iTracker;

	private JLabel ID; // label for ID.
	private JLabel created; // label for when issue was created.
	private JComboBox<String> user; // combobox of existing users.
	private JTextField text; // textfield
	private JTextField location; // location textfield.
	private JSpinner priority; // priority spinner to increment by one
								// (pos/neg).

	private int issueID = 0; // sets field issueID to 0.
	private GUI gui; // gui instance for be used.

	/**
	 * Constructor for the IssueView class.
	 * 
	 * @param issueID
	 *            the ID of the issue to be shown.
	 * @param iTracker
	 * @param gui
	 *            the gui to retrieve relevant information i.e table to use in
	 *            issueview. instantiates frame, gui and itracker and sets up
	 *            the issueView framework.
	 */
	public IssueView(int issueID, final IssueTracker iTracker) {

		this.issueID = issueID;
		this.gui = GUI.getInstance();
		this.iTracker = iTracker;
		frame = new JFrame("Issue");
		this.frame.getContentPane().setLayout(new GridLayout(7, 2));

		JLabel IDlabel = new JLabel("Issue ID:");
		this.ID = new JLabel();
		this.ID.setFont(new Font("Tahoma", Font.BOLD, 15));

		JLabel priorityLabel = new JLabel("Priority:");

		JLabel userLabel = new JLabel("Assigned User:");
		this.user = new JComboBox<String>();

		JLabel textLabel = new JLabel("Description:");
		this.text = new JTextField();

		JLabel locationLabel = new JLabel("Location:");
		this.location = new JTextField();

		this.closeButton = new JButton("Close"); // instantiates new JButton to be
											// the button to close the
											// issueview.
		this.saveButton = new JButton("Save"); // instantiates new JButton to save
											// new information.
		addActionListeners(); // calls this method so actionlisteners can be
								// used.
		frame.getContentPane().add(IDlabel); // adds the JLabel IDlabel to the
												// contentpane in frame to be be
												// the ID of the issue.
		frame.getContentPane().add(ID); // ID label to be placed opposite to
										// IDlabel with title ID shown.

		JLabel createdLabel = new JLabel("Created:");
		this.frame.getContentPane().add(createdLabel);
		this.created = new JLabel();
		this.frame.getContentPane().add(created);
		this.frame.getContentPane().add(priorityLabel);
		this.priority = new JSpinner(new SpinnerNumberModel(0, 0, 100, 1)); // instantiates
																		// a new
																		// object
																		// of
																		// JSpinner.
		this.priority.setFont(new Font("Tahoma", Font.BOLD, 15)); // sets a new font
																// to the
																// priority
																// JSpinner.
		frame.getContentPane().add(this.priority);
		frame.getContentPane().add(userLabel);
		frame.getContentPane().add(this.user);
		frame.getContentPane().add(textLabel);
		frame.getContentPane().add(this.text);
		frame.getContentPane().add(locationLabel);
		frame.getContentPane().add(this.location);
		frame.getContentPane().add(this.closeButton);
		frame.getContentPane().add(this.saveButton);

		frame.setSize(400, 200);

		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		if (issueID == -1) {
			this.issue = new Issue();
			this.issue.setID(iTracker.getIssueList().size() + 1);
		} else {
			this.issue = this.iTracker.getIssueByID(issueID);
		}
		setIssue(this.issue);
		frame.setLocationRelativeTo(this.gui);
		frame.setVisible(true);

	}

	/**
	 * @param issue
	 *            the input to set up the information about the issue in
	 *            issueView. sets the information and position to elements in
	 *            issueView. try / catch block to attempt to fill the combobox
	 *            with usernames. catch exception and prints out message to
	 *            screen.
	 */
	private void setIssue(Issue issue) {
		this.ID.setText(Integer.toString(issue.getID()));
		this.priority.setValue(issue.getPriority());
		this.text.setText(issue.getDescriptiveText());
		this.text.setCaretPosition(0);
		this.location.setText(issue.getLocation());
		this.location.setCaretPosition(0);
		if (issue.getCreatedDate() != null) { // checks that the creation date
												// on this issue is not equal to
												// null.
			this.created.setText(issue.getCreatedDate().toString()); // sets the text
																// on created
																// JLabel.
		} else {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			// get current date time with Date()

			this.created.setText(dateFormat.format(new Date()));
		}
		try {

			for (User u :this.iTracker.getSortedUsers()) {
				this.user.addItem(u.getUsername());

			}
			this.user.setSelectedItem(issue.getAssignedUser().getUsername());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

	/*
	 * void method that takes to parameters. adds actionListener to the close
	 * button. if clicked visibility is set to false and frame is removed.
	 */
	private void addActionListeners() {
		this.closeButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				frame.dispose();
			}
		});

		/*
		 * actionlistener method for the save button. when button is clicked,
		 * information is saved and updated. else JOptionPane to be shown with
		 * an error message.
		 * 
		 */
		this.saveButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (iTracker.removeIssue(issue) || issueID == -1) {
					// System.out.println("issue Removed");
					int tmpPriority = (int) priority.getValue();
					if (tmpPriority > 0 && tmpPriority <= 100) {
						iTracker.addIssue(Utils.StringToInteger(ID.getText()), tmpPriority,
								iTracker.addUser((String) user.getSelectedItem()), created.getText(), text.getText(),
								location.getText());
						Main.updateAllIssueLists();
						Utils.saveData(iTracker);

					} else {
						JOptionPane.showMessageDialog(null, "Error Saving Issue, please check input!", "Error!",
								JOptionPane.ERROR_MESSAGE);

					}
				}
				frame.setVisible(false);
				frame.dispose();

				// create issue here
			}
		});
	}
}
