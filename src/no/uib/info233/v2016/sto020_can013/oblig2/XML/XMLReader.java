
package no.uib.info233.v2016.sto020_can013.oblig2.XML;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;

import no.uib.info233.v2016.sto020_can013.oblig2.issuetracker.IssueTracker;

public class XMLReader 
{
	/**
	 * Stock reader for a utf8 xml file
	 * Gets an XML file from user and parses it into the program issueList
	 * @param file the file to be read.
	 * @param iTracker 
	 */
	public  void readXML(File file,IssueTracker iTracker)
	{
		try {

			SAXParserFactory fac = SAXParserFactory.newInstance();
			SAXParser sParser = fac.newSAXParser();

			
			if (file.exists())
			{
				InputStream inputStream= new FileInputStream(file);

				InputStreamReader inReader = new InputStreamReader(inputStream,"UTF-8");

				InputSource inSource = new InputSource(inReader);
				inSource.setEncoding("UTF-8");

				sParser.parse(inSource, new XMLHandler(iTracker));
			}
			else
			{
				System.out.println(file.getPath() + " does not exist");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}