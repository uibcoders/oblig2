/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig2.XML;

/**
 * Handles the data for the issue tables
 * Splits the data from the issues and adds them to the correct place in the tabledata
 * @author sto020
 */
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import no.uib.info233.v2016.sto020_can013.oblig2.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig2.utilities.Utils;

public class XMLHandler extends DefaultHandler {

	private String ID = null;
	private String priority = null;
	private String assigned_user = null;
	private String created = null;
	private String text = null;
	private String location = null;

	private IssueTracker iTracker = null;

	public XMLHandler(IssueTracker iTracker) {
		this.iTracker = iTracker;

	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (attributes.getLength() > 0) {

			for (int i = 0; i < attributes.getLength(); i++) {

				switch (attributes.getLocalName(i)) {
				case "id":
					ID = attributes.getValue(i);
					break;
				case "priority":
					priority = attributes.getValue(i);
					break;
				case "assigned_user":
					assigned_user = attributes.getValue(i);
					break;
				case "created":
					created = attributes.getValue(i);
					break;
				case "text":
					text = attributes.getValue(i);
					break;
				case "location":
					location = attributes.getValue(i);
					break;
				}

			}

			// add too issueList
			iTracker.addIssue(Utils.StringToInteger(ID), Utils.StringToInteger(priority), iTracker.addUser(assigned_user),
					created, text, location);
		}

	}

	public void characters(char ch[], int start, int length) throws SAXException {

		// not needed
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {

//not needed
	}

}