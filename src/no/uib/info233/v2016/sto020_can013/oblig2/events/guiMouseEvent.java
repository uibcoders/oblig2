/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig2.events;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import no.uib.info233.v2016.sto020_can013.oblig2.gui.GUI;
import no.uib.info233.v2016.sto020_can013.oblig2.main.Main;

/**
 * Class representing the MouseEvents for the GUI
 * @author sto020
 * @version 0.1.0
 */
public class guiMouseEvent extends MouseAdapter {

	private GUI gui;

	/**
	 * Constructor for the Event class
	 * 
	 * @param gui
	 */
	public guiMouseEvent() {
		this.gui = GUI.getInstance();
	}
	
	/*
	 * Method to respond on a mouse click event.
	 * @param e
	 */ 
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			if (e.getSource() == gui.getsearchDPanel().getTable()) {
				Main.editIssue(false, gui.getsearchDPanel());
			} else if (e.getSource() == gui.getAllIssuesPanel().getTable()) {

				Main.editIssue(false, gui.getAllIssuesPanel());
			} else if (e.getSource() == gui.getUserList()) {

				Main.editUser(false);

			}
		}
	}

	

}
